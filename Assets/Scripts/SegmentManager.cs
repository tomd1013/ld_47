﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentManager : MonoBehaviour
{
    [SerializeField]
    private Segment[] segments = null;

    [SerializeField]
    private float minEventWait = 3f;
    [SerializeField]
    private float maxEventWait = 20f;

    private float currentWait = 0f;

    // Start is called before the first frame update
    void Start()
    {
        currentWait = Random.Range(minEventWait, maxEventWait);
    }

    // Update is called once per frame
    void Update()
    {
        if (!GodController.IsPaused())
        {
            currentWait -= Time.deltaTime;
            if (currentWait <= 0)
            {
                currentWait = Random.Range(minEventWait, maxEventWait);
                segments[Random.Range(0, segments.Length)].Break();
            }
        }
    }
}
