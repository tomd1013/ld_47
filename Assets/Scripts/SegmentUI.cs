﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentUI : MonoBehaviour
{

    [SerializeField]
    private GameObject e = null;
    [SerializeField]
    private GameObject i = null;

    public void Start()
    {
        e.SetActive(false);
        i.SetActive(false);
    }

    public void Broken()
    {
        e.SetActive(true);
        i.SetActive(true);
    }

    public void Fixed()
    {
        e.SetActive(false);
        i.SetActive(false);
    }
}
