﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace mygame
{
    public class GameManager : MonoBehaviour
    {
        public void LoadLevel(int i)
        {
            SceneManager.LoadScene(i);
        }

        public void quit()
        {
            Quit();
        }

        public static void Quit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit(0);
#endif
        }

        public static void MainMenu()
        {
            SceneManager.LoadScene(0);
        }
    }
}

