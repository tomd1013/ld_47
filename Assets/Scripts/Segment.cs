﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour
{

    [SerializeField]
    private string segName = "";
    [SerializeField]
    private string eventName = "";
    [SerializeField]
    private SegmentUI segUI = null;
    [SerializeField]
    private float ttlBase = 6;
    private float ttl = 0;
    //private int lvl = 1;
    [SerializeField]
    private int fixClicksBase = 5;
    private int fixClicks = 0;
    private bool warned = false;

    [SerializeField]
    private AudioClip breakSound = null;
    [SerializeField]
    private AudioClip explosionSound = null;
    [SerializeField]
    private AudioClip warningSound = null;
    [SerializeField]
    private AudioClip fixEventSound = null;
    [SerializeField]
    private AudioClip fixSound = null;

    [SerializeField]
    private Light l = null;

    private bool isBroken = false;

    private AudioSource audioSource = null;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!GodController.IsPaused() && isBroken)
        {
            ttl -= Time.deltaTime;
            
            if(ttl <= 0)
            {
                // Game Over.
                Debug.Log("Game over: " + segName + " -> " + eventName);
                audioSource.PlayOneShot(explosionSound);
                GodController.Pause();
                mygame.GameManager.MainMenu();
                enabled = false;
            }
            else if (!warned && ttl <= 2)
            {
                audioSource.PlayOneShot(warningSound);
                warned = true;
            }
        }
    }

    public void Break()
    {
        if(!isBroken)
        {
            Debug.Log("Broke: " + segName);
            audioSource.PlayOneShot(breakSound);
            isBroken = true;
            ttl = ttlBase;
            fixClicks = 0;
            warned = false;
            //r.material.color = Color.red;
            l.color = Color.red;
            segUI.Broken();
        }
    }

    public void OnClick()
    {
        if(isBroken)
        {
            fixClicks++;
            audioSource.PlayOneShot(fixEventSound);
            if (fixClicks == fixClicksBase)
            {
                isBroken = false;
                ttl = 0;
                fixClicks = 0;
                audioSource.PlayOneShot(fixSound);
                segUI.Fixed();
                l.color = Color.white;
            }
        }
    }
}
