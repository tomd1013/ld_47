﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodController : MonoBehaviour
{

    private static bool isPaused = false;

    [SerializeField]
    private int rotSpeed = 50;

    private float halfWidth;

    [SerializeField]
    private GameObject pauseTxt = null;

    private enum RotationDir
    {
        LEFT, 
        RIGHT,
        NONE
    }

    RotationDir dir = RotationDir.NONE;

    // Start is called before the first frame update
    void Start()
    {
        halfWidth = Screen.width / 2.0f;
        Unpause();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(isPaused)
            {
                Unpause();
                pauseTxt.SetActive(false);
            } else
            {
                Pause();
                pauseTxt.SetActive(true);
            }
            
        }

        if (!IsPaused())
        {
            // Handle Input
            if (Input.GetMouseButton(1) || Input.GetMouseButton(2))
            {
                // Move world
                Vector3 p = Input.mousePosition;
                dir = p.x - halfWidth < 0 ? RotationDir.LEFT : RotationDir.RIGHT;
            } else if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 100.0f))
                {
                    Debug.Log("You selected the " + hit.transform.name);
                    hit.collider.gameObject.GetComponent<Segment>()?.OnClick();
                }
            }

            if (Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(2))
            {
                dir = RotationDir.NONE;
            }

            if (!dir.Equals(RotationDir.NONE))
            {
                // Controlled rotation
                int flip = 1;
                if (dir.Equals(RotationDir.LEFT)) { flip = -1; }

                transform.Rotate(Vector3.up * (flip * 3 * rotSpeed * Time.deltaTime));
            } else
            {
                // Passive rotation
                transform.Rotate(Vector3.up * (rotSpeed * Time.deltaTime));
            }
        }
    }

    public static void Pause()
    {
        isPaused = true;
        Time.timeScale = 0;
    }

    public static void Unpause()
    {
        isPaused = false;
        Time.timeScale = 1;
    }

    public static bool IsPaused()
    {
        return isPaused;
    }

    public void quit()
    {
        Quit();
    }

    public static void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit(0);
#endif
    }
}
